Use debian packaged node modules when available
 - Exclude packaged modules from transpiling
 - Remove webpack-bundle-analyzer
 - Resolve .json files
 - Add more plugins to babel.config.js
 - Add mkdirp 1.0 and loader-utils 2.0 to ensure compatibility for packaged
   modules (See #976310)
 - Use link protocol for modules with permission issues (which has a bin: entry
   in package.json) or dependencies. Use portal protocol only for modules that
   1. does not have any other dependency and 2. does not have a bin: entry
   See https://github.com/yarnpkg/berry/issues/2266
 - Even better, portal protocol tries to satisfy even devDependencies so avoid
   it
 - Modules with problems compression-webpack-plugin, copy-webpack-plugin,
   jszip, mkdirp, js-yaml, font-awesome, katex, cache-loader, webpack
 - babel-loader, cache-loader needs loader-utils 1.x and we already have 2.x and
   no compat patch
 - brace-expansion is a major version ahead in the archive
 - xterm build is broken (#980406)

--- a/config/webpack.config.js
+++ b/config/webpack.config.js
@@ -6,7 +6,6 @@
 const StatsWriterPlugin = require('webpack-stats-plugin').StatsWriterPlugin;
 const CompressionPlugin = require('compression-webpack-plugin');
 const MonacoWebpackPlugin = require('./plugins/monaco_webpack');
-const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
 const CopyWebpackPlugin = require('copy-webpack-plugin');
 const vendorDllHash = require('./helpers/vendor_dll_hash');
 
@@ -154,6 +153,8 @@
 
   entry: generateEntries,
 
+  externals: [ 'canvas'  ],
+
   output: {
     path: path.join(ROOT_PATH, 'public/assets/webpack'),
     publicPath: '/assets/webpack/',
@@ -163,13 +164,13 @@
   },
 
   resolve: {
-    extensions: ['.js', '.gql', '.graphql'],
-    modules: ['/usr/share/gitlab/node_modules','/usr/share/gitlab/node_modules/mermaid/node_modules', '/usr/lib/nodejs'],
+    extensions: ['.js', '.gql', '.graphql', '.json'],
+    modules: ['node_modules','/var/lib/gitlab/node_modules','/usr/share/gitlab/vendor/node_modules','/usr/share/gitlab/node_modules/mermaid/node_modules', '/usr/share/nodejs/markdown-it/node_modules/', '/usr/lib/nodejs', '/usr/share/nodejs'],
     alias,
   },
 
   resolveLoader: {
-    modules: ['/usr/share/gitlab/node_modules', '/usr/lib/nodejs'],
+    modules: ['node_modules','/var/lib/gitlab/node_modules', '/usr/lib/nodejs','/usr/share/nodejs'],
   },
 
   module: {
@@ -182,9 +183,13 @@
       },
       {
         test: /\.js$/,
-        exclude: path =>
+        exclude: [ path =>
           /node_modules\/(?!tributejs)|node_modules|vendor[\\/]assets/.test(path) &&
           !/\.vue\.js/.test(path),
+          /\/usr\/share\/nodejs/,
+          /\/usr\/lib\/nodejs/,
+          /\/usr\/share\/javascript/
+	],
         loader: 'babel-loader',
         options: {
           cacheDirectory: path.join(CACHE_PATH, 'babel-loader'),
@@ -261,7 +266,7 @@
       },
       {
         test: /\.(eot|ttf|woff|woff2)$/,
-        include: /node_modules\/(katex\/dist\/fonts|monaco-editor)/,
+        include: /(node_modules\/monaco-editor|\/usr\/share\/fonts\/truetype\/katex)/,
         loader: 'file-loader',
         options: {
           name: '[name].[contenthash:8].[ext]',
@@ -528,19 +533,6 @@
     // enable HMR only in webpack-dev-server
     DEV_SERVER_LIVERELOAD && new webpack.HotModuleReplacementPlugin(),
 
-    // optionally generate webpack bundle analysis
-    WEBPACK_REPORT &&
-      new BundleAnalyzerPlugin({
-        analyzerMode: 'static',
-        generateStatsFile: true,
-        openAnalyzer: false,
-        reportFilename: path.join(ROOT_PATH, 'webpack-report/index.html'),
-        statsFilename: path.join(ROOT_PATH, 'webpack-report/stats.json'),
-        statsOptions: {
-          source: false,
-        },
-      }),
-
     new webpack.DefinePlugin({
       // This one is used to define window.gon.ee and other things properly in tests:
       'process.env.IS_EE': JSON.stringify(IS_EE),
@@ -572,6 +564,7 @@
 
   node: {
     fs: 'empty', // sqljs requires fs
+    Buffer: 'mock',
     setImmediate: false,
   },
 };
--- a/package.json
+++ b/package.json
@@ -36,12 +36,12 @@
     "webpack-prod": "NODE_OPTIONS=\"--max-old-space-size=3584\" NODE_ENV=production webpack --config config/webpack.config.js"
   },
   "dependencies": {
-    "@babel/core": "^7.10.1",
-    "@babel/plugin-proposal-class-properties": "^7.10.1",
-    "@babel/plugin-proposal-json-strings": "^7.10.1",
-    "@babel/plugin-proposal-private-methods": "^7.10.1",
-    "@babel/plugin-syntax-import-meta": "^7.10.1",
-    "@babel/preset-env": "^7.10.1",
+    "@babel/core": "link:/usr/share/nodejs/@babel/core",
+    "@babel/plugin-proposal-class-properties": "link:/usr/share/nodejs/@babel/plugin-proposal-class-properties",
+    "@babel/plugin-proposal-json-strings": "link:/usr/share/nodejs/@babel/plugin-proposal-json-strings",
+    "@babel/plugin-proposal-private-methods": "link:/usr/share/nodejs/@babel/plugin-proposal-private-methods",
+    "@babel/plugin-syntax-import-meta": "link:/usr/share/nodejs/@babel/plugin-syntax-import-meta",
+    "@babel/preset-env": "link:/usr/share/nodejs/@babel/preset-env",
     "@gitlab/at.js": "1.5.5",
     "@gitlab/svgs": "1.175.0",
     "@gitlab/ui": "23.9.0",
@@ -56,29 +56,29 @@
     "apollo-link": "^1.2.14",
     "apollo-link-batch-http": "^1.2.14",
     "apollo-upload-client": "^13.0.0",
-    "autosize": "^4.0.2",
+    "autosize": "link:/usr/share/nodejs/autosize",
     "aws-sdk": "^2.637.0",
-    "axios": "^0.20.0",
+    "axios": "link:/usr/share/nodejs/axios",
     "babel-loader": "^8.0.6",
     "babel-plugin-lodash": "^3.3.4",
     "bootstrap": "4.4.1",
     "brace-expansion": "^1.1.8",
     "cache-loader": "^4.1.0",
-    "clipboard": "^1.7.1",
-    "codemirror": "^5.48.4",
+    "clipboard": "link:/usr/share/nodejs/clipboard",
+    "codemirror": "link:/usr/share/nodejs/codemirror",
     "codesandbox-api": "0.0.23",
     "compression-webpack-plugin": "^3.0.1",
     "copy-webpack-plugin": "^5.0.5",
-    "core-js": "^3.6.4",
+    "core-js": "link:/usr/share/nodejs/core-js",
     "cron-validator": "^1.1.1",
     "cropper": "^2.3.0",
     "css-loader": "^2.1.1",
-    "d3": "^5.16.0",
-    "d3-sankey": "^0.12.3",
-    "d3-scale": "^2.2.2",
-    "d3-selection": "^1.2.0",
-    "dateformat": "^3.0.3",
-    "deckar01-task_list": "^2.3.1",
+    "d3": "link:/usr/share/nodejs/d3",
+    "d3-sankey": "link:/usr/share/nodejs/d3-sankey",
+    "d3-scale": "link:/usr/share/nodejs/d3-scale",
+    "d3-selection": "link:/usr/share/nodejs/d3-selection",
+    "dateformat": "link:/usr/share/nodejs/dateformat",
+    "deckar01-task_list": "link:/usr/share/nodejs/deckar01-task_list",
     "diff": "^3.4.0",
     "document-register-element": "1.14.3",
     "dompurify": "^2.2.2",
@@ -88,41 +88,42 @@
     "emoji-unicode-version": "^0.2.1",
     "exports-loader": "^0.7.0",
     "file-loader": "^5.1.0",
-    "font-awesome": "4.7.0",
+    "font-awesome": "link:/usr/share/nodejs/font-awesome",
     "fuzzaldrin-plus": "^0.6.0",
-    "glob": "^7.1.6",
+    "glob": "link:/usr/share/nodejs/glob",
     "graphql": "^15.4.0",
     "graphql-tag": "^2.11.0",
     "immer": "^7.0.7",
     "imports-loader": "^0.8.0",
     "ipaddr.js": "^1.9.1",
     "jed": "^1.1.1",
+    "jed": "link:/usr/share/nodejs/jed",
     "jest-transform-graphql": "^2.1.0",
-    "jquery": "^3.5.0",
+    "jquery": "link:/usr/share/nodejs/jquery",
     "jquery.caret": "^0.3.1",
     "jquery.waitforimages": "^2.2.0",
-    "js-cookie": "^2.2.1",
-    "js-yaml": "^3.13.1",
-    "jszip": "^3.1.3",
-    "jszip-utils": "^0.0.2",
-    "katex": "^0.10.0",
+    "js-cookie": "link:/usr/share/nodejs/js-cookie",
+    "js-yaml": "link:/usr/share/nodejs/js-yaml",
+    "jszip": "link:/usr/share/nodejs/jszip",
+    "jszip-utils": "link:/usr/share/nodejs/jszip",
+    "katex": "link:/usr/share/nodejs/katex",
     "lodash": "^4.17.20",
     "marked": "^0.3.12",
-    "mermaid": "^8.5.2",
+    "mermaid": "link:/usr/share/nodejs/mermaid",
     "mersenne-twister": "1.1.0",
-    "minimatch": "^3.0.4",
+    "minimatch": "link:/usr/share/nodejs/minimatch",
     "miragejs": "^0.1.40",
     "mock-apollo-client": "^0.4.0",
     "monaco-editor": "^0.20.0",
     "monaco-editor-webpack-plugin": "^1.9.0",
     "monaco-yaml": "^2.5.1",
     "mousetrap": "1.6.5",
-    "pdfjs-dist": "^2.0.943",
+    "pdfjs-dist": "link:/usr/share/nodejs/pdfjs-dist",
     "pikaday": "^1.8.0",
-    "popper.js": "^1.16.1",
+    "popper.js": "link:/usr/share/nodejs/popper.js",
     "prismjs": "^1.21.0",
-    "prosemirror-markdown": "^1.3.0",
-    "prosemirror-model": "^1.6.4",
+    "prosemirror-markdown": "link:/usr/share/nodejs/prosemirror-markdown",
+    "prosemirror-model": "link:/usr/share/nodejs/prosemirror-model",
     "raphael": "^2.2.7",
     "raw-loader": "^4.0.0",
     "select2": "3.5.2-browserify",
@@ -134,33 +135,90 @@
     "style-loader": "^1.1.3",
     "swagger-ui-dist": "^3.32.4",
     "three": "^0.84.0",
-    "three-orbit-controls": "^82.1.0",
+    "three-orbit-controls": "link:/usr/share/nodejs/three-orbit-controls",
     "three-stl-loader": "^1.0.4",
-    "timeago.js": "^4.0.2",
+    "timeago.js": "link:/usr/share/nodejs/timeago.js",
     "tiptap": "^1.8.0",
     "tiptap-commands": "^1.4.0",
     "tiptap-extensions": "^1.8.0",
     "tributejs": "5.1.3",
     "url-loader": "^3.0.0",
-    "uuid": "8.1.0",
+    "uuid": "link:/usr/share/nodejs/uuid",
     "visibilityjs": "^1.2.4",
-    "vue": "^2.6.12",
+    "vue": "link:/usr/share/nodejs/vue",
     "vue-apollo": "^3.0.3",
     "vue-loader": "^15.9.3",
     "vue-router": "3.4.5",
-    "vue-template-compiler": "^2.6.12",
+    "vue-template-compiler": "link:/usr/share/nodejs/vue-template-compiler",
     "vue-virtual-scroll-list": "^1.4.4",
     "vuedraggable": "^2.23.0",
     "vuex": "^3.5.1",
     "web-vitals": "^0.2.4",
     "webpack": "^4.42.0",
-    "webpack-bundle-analyzer": "^3.6.0",
     "webpack-cli": "^3.3.11",
     "webpack-stats-plugin": "^0.3.1",
     "worker-loader": "^2.0.0",
     "xterm": "3.14.5"
   },
   "devDependencies": {
+    "@babel/plugin-transform-modules-commonjs": "^7.10.1",
+    "@gitlab/eslint-plugin": "4.0.0",
+    "@testing-library/dom": "^7.16.2",
+    "@vue/test-utils": "1.0.0-beta.30",
+    "acorn": "^6.3.0",
+    "axios-mock-adapter": "^1.15.0",
+    "babel-jest": "^26.5.2",
+    "babel-plugin-dynamic-import-node": "^2.3.3",
+    "babel-plugin-istanbul": "^6.0.0",
+    "chalk": "^2.4.1",
+    "commander": "^2.18.0",
+    "custom-jquery-matchers": "^2.1.0",
+    "docdash": "^1.0.2",
+    "eslint": "^6.8.0",
+    "eslint-import-resolver-jest": "^2.1.1",
+    "eslint-import-resolver-webpack": "^0.12.1",
+    "eslint-plugin-jasmine": "^4.1.0",
+    "eslint-plugin-no-jquery": "^2.3.0",
+    "gettext-extractor": "^3.4.3",
+    "gettext-extractor-vue": "^4.0.2",
+    "istanbul-lib-coverage": "^3.0.0",
+    "istanbul-lib-report": "^3.0.0",
+    "istanbul-reports": "^3.0.0",
+    "jasmine-core": "^2.9.0",
+    "jasmine-diff": "^0.1.3",
+    "jasmine-jquery": "^2.1.1",
+    "jest": "^26.5.2",
+    "jest-canvas-mock": "^2.1.2",
+    "jest-environment-jsdom": "^26.5.2",
+    "jest-junit": "^12.0.0",
+    "jest-util": "^26.5.2",
+    "jsdoc": "^3.5.5",
+    "jsdoc-vue": "^1.0.0",
+    "karma": "^4.2.0",
+    "karma-chrome-launcher": "^3.0.0",
+    "karma-coverage-istanbul-reporter": "^2.1.0",
+    "karma-jasmine": "^1.1.2",
+    "karma-junit-reporter": "^1.2.0",
+    "karma-mocha-reporter": "^2.2.5",
+    "karma-sourcemap-loader": "^0.3.7",
+    "karma-webpack": "^4.0.2",
+    "markdownlint-cli": "0.23.2",
+    "md5": "^2.2.1",
+    "node-sass": "^4.14.1",
+    "nodemon": "^2.0.4",
+    "pixelmatch": "^4.0.2",
+    "postcss": "^7.0.14",
+    "prettier": "1.18.2",
+    "readdir-enhanced": "^2.2.4",
+    "stylelint": "^10.1.0",
+    "stylelint-config-recommended": "^2.2.0",
+    "stylelint-scss": "^3.9.2",
+    "timezone-mock": "^1.0.8",
+    "vue-jest": "4.0.0-rc.0",
+    "webpack-dev-server": "^3.10.3",
+    "xhr-mock": "^2.5.1",
+    "yarn-check-webpack-plugin": "^1.2.0",
+    "yarn-deduplicate": "^1.1.1"
   },
   "blockedDependencies": {
     "bootstrap-vue": "https://docs.gitlab.com/ee/development/fe_guide/dependencies.html#bootstrapvue"
--- a/babel.config.js
+++ b/babel.config.js
@@ -34,9 +34,12 @@
 // include stage 3 proposals
 const plugins = [
   '@babel/plugin-syntax-import-meta',
+  '@babel/plugin-syntax-dynamic-import',
   '@babel/plugin-proposal-class-properties',
   '@babel/plugin-proposal-json-strings',
   '@babel/plugin-proposal-private-methods',
+  '@babel/plugin-proposal-optional-chaining',
+  '@babel/plugin-proposal-nullish-coalescing-operator',
   // See: https://gitlab.com/gitlab-org/gitlab/-/issues/229146
   '@babel/plugin-transform-arrow-functions',
   'lodash',
--- a/app/assets/javascripts/pdf/index.vue
+++ b/app/assets/javascripts/pdf/index.vue
@@ -1,6 +1,6 @@
 <script>
 import pdfjsLib from 'pdfjs-dist/build/pdf';
-import workerSrc from 'pdfjs-dist/build/pdf.worker.min';
+import workerSrc from 'pdfjs-dist/build/pdf.worker';
 
 import page from './page/index.vue';
 
--- a/config/application.rb
+++ b/config/application.rb
@@ -225,7 +225,7 @@
     config.assets.precompile << "illustrations/*.svg"
 
     # Import Fontawesome fonts
-    config.assets.paths << "#{config.root}/node_modules/font-awesome/fonts"
+    config.assets.paths << "/usr/share/nodejs/font-awesome/fonts"
     config.assets.precompile << "fontawesome-webfont.woff2"
     config.assets.precompile << "fontawesome-webfont.woff"
 
